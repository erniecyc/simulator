# -*- coding: utf-8 -*-

import unicodedata
from io import open
import unicodedata
import string
import re

import torch
from torch.autograd import Variable as Variable
import numpy as np

import opts

is_parallel = opts.options.p
source_data = opts.options.source
target_data = opts.options.target

dataPath = opts.options.data
max_len = opts.options.max_len
EMB = opts.options.emb
use_cuda = torch.cuda.is_available()

EPATH = opts.options.epath
EMB = opts.options.emb
EDIM = opts.options.edim
num_embeddings = 0

def compat_splitting(line):
    return line.decode('utf8').split()

# Load pretrained embedding
def loadEmb(path=EPATH):

    # Load from file
    vectors = {}
    fin = open(path, 'rb')
    for i, line in enumerate(fin):
        if i==0:
            tab = compat_splitting(line)
            num_embeddings = int(tab[0])+3
            embedding_dim = int(tab[1])
            continue
        try:
            tab = compat_splitting(line)
            vec = [float(n) for n in tab[1:]]
            word = tab[0]
            if not word in vectors:
                vectors[word] = vec
        except ValueError:
            continue
        except UnicodeDecodeError:
            continue
    fin.close()

    # Construct embedding matrix
    import torch.nn as nn
    matrix = [] # idx x vec
    stoi = {"SOS": 0, "EOS": 1, "UNK": 2} # token -> idx
    itos = {0:"SOS", 1:"EOS", 2:"UNK"}

    for idx,token in enumerate(vectors.keys()):
        matrix.append( vectors[token] )
        stoi[token] = idx
        itos[idx] = token
    
    matrix = [[0.]*EDIM]*3 + matrix

    return matrix,stoi,itos,num_embeddings

# Load embedding
if EMB:
    matrix,word2index,index2word,num_embeddings = loadEmb()

def updateMatrix(extraWordNum):
    global matrix
    # Init. domain specific words
    for i in range(extraWordNum):
        matrix += np.random.rand(1,EDIM)

    return np.array(matrix)

class batch:
    """
    This class prepares batch.

    """
    def __init__(self,io,batch=32,ratio=0.01,max_length=150):
        self.io = io
        self.ratio = ratio
        self.batch = batch
        self.done = False
        self.max_length = max_length
        self.x_source = None
        self.x_target = None 
        self.iter = self.iterbatch()

    def reset(self):
        self.iter = self.iterbatch()
        self.done = False

    def hasNext(self):
        try:
            # Load data
            self.x_source,self.x_target \
                = self.iter.__next__()
        except StopIteration as e:
            self.done = True
        return not(self.done)

    def pad(self,l):

        for e in l:
            # Padding
            e_ = torch.zeros(self.max_length, encoder.hidden_size)
            e += e_ 

    def iterbatch(self):
        """
        This class make batches of data available.

        """
        import random
        data_size = int(self.io.datasize*self.ratio)
        training_pairs = [ self.io.pair2Variables(pair) for pair in self.io.pairs ]
        random.shuffle(training_pairs) # Shuffle training set

        # Getting...
        # input_variable, target_variable, 
        # fake_input_variable, fake_target_variable

        for ndx in range(0, data_size, self.batch):

            Xs = training_pairs[ndx:min(ndx+self.batch,data_size)]

            x_source = []
            x_target = []

            for i in range(len(Xs)):
                x_source.append(Xs[i][0])
                x_target.append(Xs[i][1])
            yield torch.cat(x_source,1), torch.cat(x_target,1)

class IO:
    """
    This class prepares data.

    """
    def __init__(self):
        self.source_vocab = None 
        self.target_vocab = None
        self.discrim_vocab = None
        self.pairs = None
        self.SOS_token = 0
        self.EOS_token = 1
        self.UNK = 'UNK'
        self.datasize = 0
        self.max_len = 0

    def cleanData(self,pairs):
        new_pair = []
        max_len = 25
        for pair in pairs: 
            sent1 = pair[0].split(' ')
            sent2 = pair[1].split(' ')
            if len(sent1)>max_len:
                sent1 = sent1[:max_len]
            if len(sent2)>max_len:
                sent2 = sent2[:max_len]
            new_pair.append( (' '.join(sent1),' '.join(sent2)) )
        return new_pair

    def prepareData(self, source, target, reverse=False):
        global matrix

        source_vocab, target_vocab, pairs = self.readData(source, target, reverse)

        # Truncate to length 25
        pairs = self.cleanData(pairs)

        print("Read %s sentence pairs" % len(pairs))
        print("Counting words...")
        for pair in pairs:
            source_vocab.addSentence(pair[0])
            target_vocab.addSentence(pair[1])

        self.max_len = max(source_vocab.max_len,target_vocab.max_len)+1

        if EMB:
            # Update emb. matrix
            extraWordNum = source_vocab.n_words-num_embeddings
            matrix = updateMatrix(extraWordNum)

        print("Total vocab size:",source_vocab.n_words)
        print("Maximum length:",self.max_len)

        self.source_vocab = source_vocab
        self.target_vocab = target_vocab
        self.pairs = pairs
        self.datasize = len(self.pairs)

    def generateZ(self):
        import random
        source = np.random.randint(0,self.source_vocab.n_words, size=(self.max_len,1)) 
        source = Variable(torch.LongTensor(source))
        target = np.random.randint(0,self.target_vocab.n_words, size=(self.max_len,1))
        target = Variable(torch.LongTensor(target))        
        return (source,target)

    def sent2Variable(self,vocab,sentence):
        indices = [vocab.word2index[word] if word in vocab.word2index.keys()
                    else vocab.word2index[self.UNK] for word in sentence ]
        result = Variable(torch.LongTensor(indices+np.ones(self.max_len-len(indices))).view(-1, 1))
        if use_cuda:
            return result.cuda()
        else:
            return result
        return result

    def sentence2Indices(self, vocab, sentence):
        indices = [vocab.word2index[word] if word in vocab.word2index.keys() 
                    else vocab.word2index[self.UNK] for word in sentence.split(' ')]
        return indices+[1]*(self.max_len-len(indices)-1)

    def sentence2Variable(self, vocab, sentence):
        indices = self.sentence2Indices(vocab, sentence)
        indices.append(self.EOS_token)
        result = Variable(torch.LongTensor(indices).view(-1, 1))
        if use_cuda:    
            return result.cuda()
        else:
            return result

    def pair2Variables(self, pair):
        source = self.sentence2Variable(self.source_vocab, pair[0])
        target = self.sentence2Variable(self.target_vocab, pair[1])
        return (source, target)

    def convert2turns(self,lines):
        return [ '\t'.join( [ l[1] for l in lines[i:i+2] ] ) for i in range(len(lines)) ]

    def readMovies(self,dataPath):
        """ Read movies transcripts """

        # Read the file 
        lines = []
        for line in open(dataPath,'rb').readlines():
            try:
                fields = line.decode('utf-8').split('+++$+++')
                name = fields[3]
                sent = fields[-1].replace('\n','').strip()
                lines.append( (name,sent) )
            except Exception:
                continue
        # Form pairs
        pairs = [[self.normalizeString(s) for s in l.split('\t')] 
                            for l in self.convert2turns(lines) if '\t' in l]
        return pairs

    def readQA(self,dataPath):
        # Read QA file
        pairs = []
        for line in open(dataPath,'r').readlines():
            try:
                fields = line.split('\t')
                question = fields[0].strip()
                answer = fields[1].strip()
                if '0' in fields[2].strip(): continue
                pairs.append( [self.normalizeString(question),
                               self.normalizeString(answer)] )
            except Exception:
                print('Exception!')
                continue
        # Form pairs
        return pairs

    def readParallel(self,source,target):
        """ Read parallel dataset """
        pairs = []
        for s,t in zip(open(source,'r').readlines(),
                       open(target,'r').readlines()):
            pairs.append( [self.normalizeString(s),
                           self.normalizeString(t)] )
        return pairs

    def readData(self, source, target, reverse=False):
        print("Reading lines...")

        # Read different datasets
        if is_parallel:
            pairs = self.readParallel(source_data,target_data)
        else:
            if 'movie' in dataPath:
                pairs = self.readMovies(dataPath)
            else:
                pairs = self.readQA(dataPath)

        # Reverse pairs, make Vocab instances
        if reverse:
            pairs = [list(reversed(p)) for p in pairs]
            source_vocab = Vocab(target)
            target_vocab = Vocab(source)
        else:
            source_vocab = Vocab(source)
            target_vocab = Vocab(target)

        return source_vocab, target_vocab, pairs

    def unicodeToAscii(self,s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
        )

    # Lowercase, trim, and remove non-letter characters
    def normalizeString(self,s):
        s = self.unicodeToAscii(s.lower().strip())
        s = re.sub(r"([.!?])", r" \1", s)
        s = re.sub(r"[^a-zA-Z.!?]+", r" ", s)
        return s

    def pad_indices(self,max_length,original):
        padded_z = Variable(torch.ones(max_length, 1)) # init with 1 for EOS
        for ei in range(max_length):
            if ei>len(original)-1: break
            padded_z[ei] = original[ei]
        return padded_z

class Vocab:
    def __init__(self, name):
        self.name = name
        self.word2index = word2index if EMB else {'UNK': 2}
        self.index2word = index2word if EMB else {0: "SOS", 1: "EOS", 2: "UNK"}
        self.n_words = num_embeddings if EMB else 3
        self.max_len = 0 

    def addSentence(self, sentence):

        # Update max length
        l = len(sentence.split(' '))
        if l>self.max_len:
            self.max_len = l

        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.index2word[self.n_words] = word
            self.n_words += 1

# For testing 
if __name__ == "__main__":

    embedding,pretrained_embeddings_matrix,stoi = loadEmb()












