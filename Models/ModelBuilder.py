# -*- coding: utf-8 -*-

import random
import math
import copy

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim

import Models.Models as m
import Models.Node as N
import opts

BATCH = opts.options.batch
use_cuda = torch.cuda.is_available()

def encode(io, input_variable, encoder, max_length):

    input_length = input_variable.size()[0] # Use max sent length
    batch = min(input_variable.size()[1],BATCH) # Choose smaller batch

    # Padding
    encoder_outputs = Variable(torch.zeros(max_length, encoder.hidden_size))
    encoder_outputs = encoder_outputs.cuda() if use_cuda else encoder_outputs

    # Encoding
    encoder_hidden = encoder.initHidden()

    # encoder_output : [sent_len,batch,hidden_size]
    # encoder_hidden : [1,batch,hidden_size]
    encoder_output, encoder_hidden = encoder(input_variable,encoder_hidden,batch)

    # Decoding
    decoder_input = Variable(torch.LongTensor([[io.SOS_token]*batch]))
    decoder_input = decoder_input.cuda() if use_cuda else decoder_input
    decoder_hidden = encoder_hidden

    return decoder_input, decoder_hidden, encoder_output, encoder_outputs

def beamSearch(io, decoder, decoder_input, decoder_hidden, 
           encoder_output, encoder_outputs, max_length):

    # Define beam size
    k = 1

    # Init. curr_stack
    ni = io.SOS_token
    n = N.Node(score=0,
               stri=io.target_vocab.index2word[ni],
               ni=ni)
    curr_stack = [n]

    for di in range(max_length):

        # Continue from last beam 
        while curr_stack:

            prev = curr_stack.pop()
            prev_score = prev.score
            prev_stri = prev.stri
            prev_ni = prev.ni

            decoder_input = Variable(torch.LongTensor([[prev_ni]]))
            decoder_input = decoder_input.cuda() if use_cuda else decoder_input

            # Expand Children
            decoder_output, decoder_hidden = \
                decoder(decoder_input, decoder_hidden, 
                        encoder_output, encoder_outputs) 

            # if previous node has ended, add back & continue
            if prev.is_end:
                tmp_stack.append(prev)
                continue

            # Add all nodes to tmp_stack
            tmp_stack = []
            vocab_probs = decoder_output[0].data.numpy()

            for ni,score in enumerate(vocab_probs):

                # Compute new scores
                new_score = prev_score + score
                new_token = io.target_vocab.index2word[ni]
                new_ni = ni

                n = N.Node(score=new_score,
                           stri=prev_stri+' '+new_token,
                           ni=ni)

                if ni == io.EOS_token:
                    n.is_end = True

                tmp_stack.append(n)

            # Sort tmp_stack
            tmp_stack.sort(key=lambda n: n.score, reverse=True)

        # Get beam size
        curr_stack = copy.copy(tmp_stack[:k])

        # Check if all sequences ends 
        if all( n.is_end for n in curr_stack ):
            break

    response = curr_stack[0].stri.replace('EOS','').replace('SOS','').replace('UNK','')

    '''
    import nltk.data
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    response = tokenizer.tokenize(response)[0]
    '''

    return response

def decode(io, decoder, criterion, target_variable, decoder_input, 
           decoder_hidden, encoder_output, encoder_outputs):

    target_length = target_variable.size()[0]

    # Collect model output sequence
    output_list = []

    loss = 0

    # Teacher forcing: Feed the target as the next input
    for di in range(target_length):
        decoder_output, decoder_hidden = decoder(decoder_input, decoder_hidden, 
                                                 encoder_output, encoder_outputs)
        loss += criterion(decoder_output, target_variable[di])
        label = target_variable[di].unsqueeze(0) # add batch dim

        topv, topi = decoder_output.data.topk(1)
        ni = topi[0][0]
        output_list += [io.target_vocab.index2word[ni]]

        decoder_input = label # Teacher forcing

        if ni == io.EOS_token:
            break

    response = ' '.join(output_list[:-1])
        
    # [sent_len,batch]
    label = target_variable
    ave_loss = loss.data[0]/target_length

    print(response)


    return ave_loss, loss, response

def indices2emb(decoder,indices):
    # Encoding
    token_emb = decoder.embedding(indices)
    return token_emb

def generate_response(io,input_variable,encoder,decoder,max_length):
    """ Generate response """

    # Encoder networks
    decoder_input, decoder_hidden, encoder_output, encoder_outputs = \
                            encode(io, input_variable, encoder, max_length)
    # Decoder networks
    response = beamSearch(io, decoder, decoder_input, decoder_hidden, 
                                encoder_output, encoder_outputs, max_length)

    return response

def generate(io,input_variable,target_variable,encoder,decoder,criterion,max_length):
    """ Generate output """

    # Encoder networks
    decoder_input, decoder_hidden, encoder_output, encoder_outputs = \
                            encode(io, input_variable, encoder, max_length)
    # Decoder networks
    ave_loss,loss, response = decode(io, decoder, criterion, target_variable, decoder_input, \
                                     decoder_hidden, encoder_output, encoder_outputs)

    gen_dict = { 'ave_loss': ave_loss,
                 'loss': loss,
                 'response': response }

    return gen_dict




