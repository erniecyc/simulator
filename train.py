# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import os

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim

import IO
import opts
import Models.Models as m
import Models.Loss as l
import Models.ModelBuilder as mb
import report

use_cuda = torch.cuda.is_available()

# Get options
TOTAL_EPOCHS = opts.options.epochs
BATCH = opts.options.batch
MAX_LEN = opts.options.max_len
RNN_NUM = opts.options.rnnlayers
PRINT_EVERY = opts.options.print_every
LR = opts.options.lr
EMB = opts.options.emb
EDIM = opts.options.edim


def main():

    io = IO.IO()

    # Prepares data
    io.prepareData('Source', 'Target')

    # Load pre-trained embeddings
    embed_weights = IO.matrix if EMB else None

    # Generator
    if EMB:
        emb_num = IO.num_embeddings
    else:
        emb_num = 0

    hidden_size = EDIM
    num_embeddings_src = max(io.source_vocab.n_words,emb_num)
    num_embeddings_tgt = max(io.target_vocab.n_words,emb_num)

    encoder = m.EncoderRNN(num_embeddings_src, hidden_size, embed_weights)
    decoder = m.DecoderRNN(hidden_size, num_embeddings_tgt, embed_weights,
                            1, dropout_p=0.001, max_length=io.max_len)

    if use_cuda:
        encoder = encoder.cuda()
        decoder = decoder.cuda()

    trainer(io, encoder, decoder, total_epochs=TOTAL_EPOCHS)


def trainer(io, encoder, decoder, total_epochs=1000):
    r"""  
    This function defines the training procedure.
    
    Args: 
        io(IO): IO object.
        encoder(EncoderRNN): encoder object.
        decoder(AttnDecoderRNN): decoder object.
        total_epochs(int): number of epochs.

    Returns:
        None

    """ 

    encoder_optimizer = optim.Adam(encoder.parameters(), lr=LR)
    decoder_optimizer = optim.Adam(decoder.parameters(), lr=LR)
    optimizers = (encoder_optimizer,decoder_optimizer)

    criterion = nn.NLLLoss()

    start_epoch = 1

    data_size = max(int(io.datasize/BATCH)+1,2)
    print('Total data points {}.'.format(io.datasize))
    print('Batch size {}.'.format(BATCH))

    b = IO.batch(io,batch=BATCH,ratio=1)

    for epoch in range(start_epoch,total_epochs+1):

        print('Epoch {}/{}'.format(epoch,total_epochs))

        training_pairs = random.shuffle([io.pair2Variables(pair) for pair in io.pairs])

        start = time.time()
        plot_losses = []
        print_loss_total = 0  

        progress = 0
        while b.hasNext():  
            loss = step(io, b.x_source, b.x_target, 
                        encoder, decoder, optimizers, criterion, io.max_len)

            print_loss_total += loss
            progress += 1

        # Reset
        b.reset()

        # Report
        print_loss_avg = print_loss_total / data_size
        print( str('%s (%d %d%%) Gen %.4f' % (report.timeSince(start, progress / data_size),
                                     progress, progress / data_size * 100, print_loss_avg))
                                     .center(os.get_terminal_size().columns) )

        if epoch==TOTAL_EPOCHS-1:
            checkpoint = {
                'opt': opts.options,
                'epoch': epoch,
                'io': io,
                'encoder': encoder,
                'decoder': decoder, 
                'encoder_optimizer': encoder_optimizer,
                'decoder_optimizer': decoder_optimizer
            }
            torch.save(checkpoint,open(opts.options.model+'/model_'+str(epoch),'wb'))

def step(io, input_variable, target_variable, encoder, 
         decoder, optimizers, criterion, max_length):
    r"""
    This function steps through the entire encoder-decoder model.

    Returns:
        ave_loss(int): average loss over sequence length.
    """

    encoder_optimizer,decoder_optimizer = optimizers

    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    # Generation
    gen_dict = mb.generate(io,input_variable,target_variable,
                       encoder,decoder,criterion,max_length)

    loss = gen_dict['loss']
    ave_loss = gen_dict['ave_loss']

    # Backprop
    loss.backward()

    # Step optimizer / change lr
    encoder_optimizer.step()
    decoder_optimizer.step()
    
    return ave_loss


if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))




