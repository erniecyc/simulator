



def multiply(sent):
    """ Paraphrasing tool """
    import subprocess
    sent = sent
    command = "echo \""+sent+"\" | \
              ./paraphrase_tool/ace -g paraphrase_tool/erg-1212-osx-0.9.26.dat \
              -1T 2>/dev/null | ./paraphrase_tool/ace \
              -g paraphrase_tool/erg-1212-osx-0.9.26.dat -e"
    output = subprocess.check_output(command,shell=True)
    paraphrases = output.decode('utf-8').split('\n')[:-2]
    if len(paraphrases)==0: return [sent.replace('\n','').strip()]
    return paraphrases


#print(multiply('Please tell me the date, time, location, and the event.'))

sys_exp = open('datasets/parallel/sys_exp','a')
user_exp = open('datasets/parallel/user_exp','a')

for sys,user in zip(open('datasets/parallel/sys_seeds').readlines(), \
                    open('datasets/parallel/user_seeds').readlines()):
    paraphrases = multiply(sys)
    user_repeats = [ user ] * len(paraphrases)
    for sys,user in zip(paraphrases,user_repeats):
        sys_exp.write(sys+'\n')
        user_exp.write(user)

sys_exp.close()
user_exp.close()






