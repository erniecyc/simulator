# Simulator

This is a general-purposed SEQ2SEQ simulator which simulates dialog conversation.

To-Do:

- [x] Set-up of simulation
- [x] Attention
- [X] Jesus quote
- [X] Load pre-trained embedding
- [ ] Re-inforcement learning
- [ ] Adversarial training




Requirements on Python 3.5.4:

```

pip install -r requirements.txt

numpy==1.13.1
torch==0.2.0.post4

```


To train, simply type:

```
usage: train.py [-h] [-data DATA] [-model MODEL] [-p] [-emb] [-epath EPATH]
                [-edim EDIM] [-source SOURCE] [-target TARGET]
                [-epochs EPOCHS] [-patience PATIENCE]
                [-print_every PRINT_EVERY] [-lr LR]
                [-start_checkpoint_at START_CHECKPOINT_AT] [-max_len MAX_LEN]
                [-batch BATCH] [-rnnlayers RNNLAYERS] [-resume]
                [-checkpoint CHECKPOINT]

Simulator is a system that outputs dialog.

optional arguments:
  -h, --help            show this help message and exit
  -data DATA            Path to the data.
  -model MODEL          Path to save model.
  -p                    Specify if parallel datasets will be read.
  -emb                  Specify if pre-trained embedding should be loaded.
  -epath EPATH          Path to emebdding.
  -edim EDIM            Number of embedding dimensions.
  -source SOURCE        Path to the source data.
  -target TARGET        Path to the target data.
  -epochs EPOCHS        Maximum number of epochs.
  -patience PATIENCE    Number of epochs to wait.
  -print_every PRINT_EVERY
                        Number of epochs to print.
  -lr LR                Learning rate.
  -start_checkpoint_at START_CHECKPOINT_AT
                        Number of epochs to start saving checkpoints.
  -max_len MAX_LEN      Maximum sentence length.
  -batch BATCH          Batch size.
  -rnnlayers RNNLAYERS  Number of RNN layers.
  -resume               To resume from previous checkpoint or not.
  -checkpoint CHECKPOINT
                        Specify checkpoint file path.

```

For user-interactive mode:

```
python test.py


usage: test.py [-h] [-data DATA] [-model MODEL] [-p] [-emb] [-epath EPATH]
               [-edim EDIM] [-source SOURCE] [-target TARGET] [-epochs EPOCHS]
               [-patience PATIENCE] [-print_every PRINT_EVERY] [-lr LR]
               [-start_checkpoint_at START_CHECKPOINT_AT] [-max_len MAX_LEN]
               [-batch BATCH] [-rnnlayers RNNLAYERS] [-resume]
               [-checkpoint CHECKPOINT]

```

