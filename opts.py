# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Simulator is a system that outputs dialog.')

# Data and loading options
parser.add_argument('-data', required=False,
                             help='Path to the data.',
                             default='rescoring/')
parser.add_argument('-model', required=False,
                             help='Path to save model.',
                             default='saved_models')
parser.add_argument('-p', action='store_true',
                             help='Specify if parallel datasets will be read.',
                             default=False)
parser.add_argument('-emb', action='store_true',
                             help='Specify if pre-trained embedding should be loaded.',
                             default=False)
parser.add_argument('-epath', required=False,
                             help='Path to emebdding.',
                             type=str,
                             default='embedding/glove.6B.50d.txt')
parser.add_argument('-edim', required=False,
                             help='Number of embedding dimensions.',
                             type=int,
                             default=50)
parser.add_argument('-source', required=False,
                             help='Path to the source data.',
                             default='rescoring/')
parser.add_argument('-target', required=False,
                             help='Path to the target data.',
                             default='rescoring/')
parser.add_argument('-epochs', required=False,
                             help='Maximum number of epochs.',
                             type=int,
                             default=1000)
parser.add_argument('-patience', required=False,
                             help='Number of epochs to wait.',
                             type=int,
                             default=5)
parser.add_argument('-print_every', required=False,
                             help='Number of epochs to print.',
                             type=int,
                             default=100)
parser.add_argument('-lr', required=False,
                             help='Learning rate.',
                             type=int,
                             default=0.0001)
parser.add_argument('-start_checkpoint_at', required=False,
                             help='Number of epochs to start saving checkpoints.',
                             type=int,
                             default=3)
parser.add_argument('-max_len', required=False,
                             help='Maximum sentence length.',
                             type=int,
                             default=150)
parser.add_argument('-batch', required=False,
                             help='Batch size.',
                             type=int,
                             default=32)
parser.add_argument('-rnnlayers', required=False,
                             help='Number of RNN layers.',
                             type=int,
                             default=1)
parser.add_argument('-resume', action='store_true',
                             help='To resume from previous checkpoint or not.')
parser.add_argument('-checkpoint', required=False,
                             help='Specify checkpoint file path.',
                             default='model_9')

options = parser.parse_args()




