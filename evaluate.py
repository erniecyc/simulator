# -*- coding: utf-8 -*-

import torch
from torch.autograd import Variable

use_cuda = torch.cuda.is_available()


def evaluate(io, encoder, decoder, sentence, max_length):

    input_variable = io.sentence2Variable(io.input_source, sentence)
    input_length = input_variable.size()[0]
    encoder_hidden = encoder.initHidden()

    encoder_outputs = Variable(torch.zeros(max_length, encoder.hidden_size))
    encoder_outputs = encoder_outputs.cuda() if use_cuda else encoder_outputs

    for ei in range(input_length):
        encoder_output, encoder_hidden = encoder(input_variable[ei],
                                                 encoder_hidden)
        encoder_outputs[ei] = encoder_outputs[ei] + encoder_output[0][0]

    decoder_input = Variable(torch.LongTensor([[io.SOS_token]]))  # SOS
    decoder_input = decoder_input.cuda() if use_cuda else decoder_input

    decoder_hidden = encoder_hidden

    decoded_words = []
    decoder_attentions = torch.zeros(max_length, max_length)

    # Beam search
    for di in range(max_length):

        decoder_output, decoder_hidden, decoder_attention = decoder(
            decoder_input, decoder_hidden, encoder_output, encoder_outputs)

        decoder_attentions[di] = decoder_attention.data
        topv, topi = decoder_output.data.topk(1)
        ni = topi[0][0]

        if ni == io.EOS_token:
            decoded_words.append('<EOS>')
            break
        else:
            decoded_words.append(io.output_target.index2word[ni])
        
        decoder_input = Variable(torch.LongTensor([[ni]]))
        decoder_input = decoder_input.cuda() if use_cuda else decoder_input

    return decoded_words, decoder_attentions[:di + 1]


def evaluateRandomly(encoder, decoder, n=10):
    for i in range(n):
        pair = random.choice(pairs)
        print('>', pair[0])
        print('=', pair[1])
        output_words, attentions = evaluate(encoder, decoder, pair[0])
        output_sentence = ' '.join(output_words)
        print('<', output_sentence)
        print('')


# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Title generator is a system that generates titles.')

# Data and loading options
parser.add_argument('-data', required=False,
                             help='Path to the data.',
                             default='rescoring/')
parser.add_argument('-model', required=False,
                             help='Path to save model.',
                             default='models/model_1')

opts = parser.parse_args()

#evaluateRandomly(encoder1, attn_decoder1)

checkpoint = torch.load(opts.model)

encoder = checkpoint['encoder']
decoder = checkpoint['decoder']
chk_opt = checkpoint['opt']
io = checkpoint['io']


def reply(sent,io,encoder,decoder,max_len):
    output_words, attentions = evaluate(io, encoder, decoder, sent, max_len)
    return ' '.join(output_words[:-1])

while True:
    text = input("Question: ")
    print(reply(text,io,encoder,decoder,chk_opt.max_len))


'''
import matplotlib.pyplot as plt
plt.matshow(attentions.numpy())
'''




