# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import opts

use_cuda = torch.cuda.is_available()
EMB = opts.options.emb


class PositionalEncoding(nn.Module):
    r""" 
    Credits to https://github.com/OpenNMT/OpenNMT-py/blob/master/onmt/modules/Embeddings.py
    
    """
    def __init__(self, dropout, dim, max_len=5000):
        pe = torch.arange(0, max_len).unsqueeze(1).expand(max_len, dim)
        div_term = 1 / torch.pow(10000, torch.arange(0, dim * 2, 2) / dim)
        pe = pe * div_term.expand_as(pe)
        pe[:, 0::2] = torch.sin(pe[:, 0::2])
        pe[:, 1::2] = torch.cos(pe[:, 1::2])
        pe = pe.unsqueeze(1)
        super(PositionalEncoding, self).__init__()
        self.register_buffer('pe', pe)
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, emb):
        emb = emb + Variable(self.pe[:emb.size(0), :1, :emb.size(2)]
                             .expand_as(emb), requires_grad=False)
        emb = self.dropout(emb)
        return emb

class Embeddings(nn.Module):
    r"""
    This is the customized embedding layer. 
    It includes:
        1) positional encoding
        2) MLP

    output : [seq_len, batch, hidden_size]

    """
    def __init__(self, input_size, hidden_size):
        super(Embeddings, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(input_size, hidden_size)
        self.posEncoder = PositionalEncoding(dropout=0.1,dim=hidden_size)
        self.mlp = nn.Linear(hidden_size, hidden_size)

    def forward(self, input):
        # input : [sent_len,batch]
        embedded = self.embedding(input)
        embedded = self.posEncoder(embedded)
        output = self.mlp(embedded)
        return output

class EncoderRNN(nn.Module):
    r"""
    This is an encoder which takes as input indices sequences to 
    return output vector and last hidden state.

    """

    def __init__(self, input_size, hidden_size, embed_weights=None, n_layers=1, batch=32, num_directions=2):
        super(EncoderRNN, self).__init__()

        self.n_layers = n_layers
        self.hidden_size = hidden_size
        self.batch = batch
        self.num_directions = num_directions

        self.embedding = Embeddings(input_size, hidden_size)

        if EMB:
            self.embedding.weight = nn.Parameter(torch.from_numpy(embed_weights))

        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=self.n_layers)

    def forward(self, input, hidden, batch):
        r"""
        Returns:
            output : [seq_len, batch, hidden_size * num_directions]
            hidden : [num_layers, batch, hidden_size]
        """
        hidden = hidden.repeat(1,input.size()[1],1) # [num_layers, batch, hidden_size]
        output = self.embedding(input) # [seq_len, batch, input_size]
        output, hidden = self.gru(output, hidden) 
        return output, hidden

    def initHidden(self):
        r""" 
        Return:
            result : (num_layers * num_directions, batch, hidden_size) 
        """
        result = Variable(torch.zeros(self.n_layers, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result

class DecoderRNN(nn.Module):
    r"""
    Decoder that takes as input.
    Features:
        Attention mechanism

    """
    def __init__(self, hidden_size, output_size, embed_weights=None, 
                n_layers=1, dropout_p=0.1, max_length=10):
        super(DecoderRNN, self).__init__()
        self.n_layers = n_layers
        self.hidden_size = hidden_size
        self.max_length = max_length

        self.embedding = nn.Embedding(output_size, hidden_size)

        if EMB:
            self.embedding.weight.data.copy_(torch.from_numpy(embed_weights))

        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=self.n_layers)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax()

        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)


    def forward(self, input, hidden, encoder_output, encoder_outputs):
        embedded = self.embedding(input)

        # Attention layer
        attn_weights = F.softmax(
            self.attn(torch.cat((embedded[0], hidden[0]), 1)))
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                 encoder_outputs.unsqueeze(0))

        output = torch.cat((embedded[0], attn_applied[0]), 1)
        output = self.attn_combine(output).unsqueeze(0)

        # RNN layer
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0])) 
        # output : [batch, output_size]
        return output, hidden

    def initHidden(self):
        result = Variable(torch.zeros(self.n_layers, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result
