# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import os
import argparse

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim

import IO
import opts
import Models.Models as m
import Models.Loss as l
import Models.ModelBuilder as mb
import report


# Data and loading options
parser = argparse.ArgumentParser(description='Options to determine ways to test bot.')
parser.add_argument('-token', required=False,
                             help='Set access token.',
                             default='b74e4fd005464d24b38b7812ca0a9ee4')
parser.add_argument('-checkpoint', required=False,
                             help='Specify checkpoint file path.',
                             default='saved_models/model_999')
options = parser.parse_args()

def main():

    checkpoint = torch.load(options.checkpoint)
    encoder = checkpoint['encoder']
    decoder = checkpoint['decoder']
    io = checkpoint['io']
    opt = checkpoint['opt']
    MAX_LEN = opts.options.max_len

    while True:
        user_response = input()
        input_variable = io.sentence2Variable(io.source_vocab,user_response)
        response = mb.generate_response(io,input_variable,encoder,decoder,io.max_len)
        print(response)

if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))






