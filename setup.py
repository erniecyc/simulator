from setuptools import setup

setup(name='simBoli',
      version='0.1',
      description='SEQ2SEQ Simulator',
      url='https://erniecyc@bitbucket.org/erniecyc/simulator.git',
      author='Ernie Chang',
      author_email='cyc025@uw.edu',
      license='MIT',
      packages=['simulator'],
      zip_safe=False)
